package talents

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/krichprollsch/crew/api"
)

// Fetch returns the talents saved by the server.
//
// TODO the function retrieve data from DB and encode them in json format.
// Maybe there is a smarter way to retrieve formatted json data from db which
// can be streamed directly to the client.
func Fetch(mainctx context.Context, db *mongo.Database) api.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) error {
		ctx := r.Context()

		// handle query parameters.
		var (
			limit int
			page  int
			err   error
		)
		if v := r.URL.Query().Get("limit"); v != "" {
			limit, err = strconv.Atoi(v)
			if err != nil {
				return api.BadRequest("invalid limit")
			}
		}

		if v := r.URL.Query().Get("page"); v != "" {
			page, err = strconv.Atoi(v)
			if err != nil {
				return api.BadRequest("invalid page")
			}
		}

		// A Limit is required if page is send.
		if page != 0 && limit == 0 {
			return api.BadRequest("invalid limit")
		}

		opts := options.Find()
		if limit > 0 {
			opts.SetLimit(int64(limit))
			// page starts at 0
			opts.SetSkip(int64(page * limit))
		}

		coll := db.Collection(Coll)
		// TODO handle no rows error.
		cur, err := coll.Find(ctx, bson.D{}, opts)
		if err != nil {
			return fmt.Errorf("find talents: %w", err)
		}
		// Use background context to be sure to close the cursor even if the
		// http clinet has gone.
		defer cur.Close(mainctx)

		var results []Talent
		if err = cur.All(ctx, &results); err != nil {
			return fmt.Errorf("fetch talents: %w", err)
		}

		w.Header().Set("Content-Type", "application/json")

		enc := json.NewEncoder(w)
		if err := enc.Encode(results); err != nil {
			return fmt.Errorf("json encode talents: %w", err)
		}

		return nil
	}
}

// Create is an http handler for talent creation.
func Create(_ context.Context, db *mongo.Database) api.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) error {
		ctx := r.Context()

		if r.Header.Get("Content-type") != "application/json" {
			return api.BadRequest("invalid content-type")
		}

		// Parse the JSON body
		var tlnt Talent
		dec := json.NewDecoder(r.Body)
		if err := dec.Decode(&tlnt); err != nil {
			return api.BadRequest("invalid body")
		}

		// Check the data validity.
		if err := tlnt.Valid(); err != nil {
			return api.BadRequest(err.Error())
		}

		// Insert the data into db.
		coll := db.Collection(Coll)
		_, err := coll.InsertOne(ctx, tlnt)
		if err != nil {
			return fmt.Errorf("insert talent: %w", err)
		}

		w.WriteHeader(http.StatusCreated)
		return nil
	}
}
