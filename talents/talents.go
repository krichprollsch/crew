package talents

import (
	"errors"
	"regexp"
)

// Coll is the MongoDB collection name.
const Coll = "talents"

type Id string

var (
	idPattern    = regexp.MustCompile("[a-z0-9]{20,}")
	ErrInvalidId = errors.New("invalid id")
)

func (id Id) Valid() error {
	if !idPattern.MatchString(string(id)) {
		return ErrInvalidId
	}

	return nil
}

// TODO add Valid func
type URL string

type Stage string

var ErrInvalidStage = errors.New("invalid stage")

const (
	StageContacted       Stage = "👻 Contacted"
	StageHired           Stage = "🍾 Hired"
	StageNew             Stage = "✨ New"
	StageNotNow          Stage = "⏰ Not Now"
	StageOfferSent       Stage = "📝 Offer Sent"
	StageOnsiteInterview Stage = "🤝 Onsite Interview"
	StagePhoneScreen     Stage = "📞 Phone Screen"
	StageRejected        Stage = "⛔ Rejected"
)

func (s Stage) Valid() error {
	switch s {

	case StageContacted, StageHired, StageNew, StageNotNow, StageOfferSent,
		StageOnsiteInterview, StagePhoneScreen, StageRejected:
		return nil
	}
	return ErrInvalidStage
}

type Talent struct {
	Id Id `json:"id" bson:"_id"`

	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Picture   URL    `json:"picture,omitempty"`
	Job       string `json:"job"`
	Location  string `json:"location"`

	Linkedin URL `json:"linkedin,omitempty"`
	Github   URL `json:"github,omitempty"`
	Twitter  URL `json:"twitter,omitempty"`

	Tags  []string `json:"tags,omitempty"`
	Stage Stage    `json:"stage"`
}

var ErrInvalidTalent = errors.New("invalid talent")

// Valid ensures the talent has correct values only.
// TODO add more checks.
func (t Talent) Valid() error {
	if err := t.Id.Valid(); err != nil {
		return err
	}

	if t.FirstName == "" {
		return ErrInvalidTalent
	}
	if t.LastName == "" {
		return ErrInvalidTalent
	}
	if t.Job == "" {
		return ErrInvalidTalent
	}
	if t.Location == "" {
		return ErrInvalidTalent
	}

	if err := t.Stage.Valid(); err != nil {
		return err
	}

	return nil
}
