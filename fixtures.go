package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/krichprollsch/crew/talents"
)

const fixturesURL = "https://hiring.crew.work/v1/talents"

// fixtures loads talents data from a public exposed HTTP API and insert them
// into mongoDB.
// Talents mongoDB collection is dropped before fixtures insertion.
func fixtures(ctx context.Context, db *mongo.Database) error {
	// Request fixtures endpoint.
	req, err := http.NewRequestWithContext(ctx, "GET", fixturesURL, nil)
	if err != nil {
		return fmt.Errorf("new req: %w", err)
	}

	// TODO use better conf defaults for the HTTP cli.
	cli := &http.Client{}
	resp, err := cli.Do(req)
	if err != nil {
		return fmt.Errorf("do req: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New("bad status code")
	}

	// Parse response body.
	var tlnts []talents.Talent

	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&tlnts); err != nil {
		return fmt.Errorf("parse data: %w", err)
	}

	// Prepare talents for MongoDB insertion.
	// Maybe there is a smarter way to insert the slice, but I can't figure how
	// for now.
	documents := make([]interface{}, len(tlnts))
	for i, v := range tlnts {
		documents[i] = interface{}(v)
	}

	coll := db.Collection(talents.Coll)

	// TODO find a way to use a transaction with MongoDB.
	// Drop existing data.
	if err := coll.Drop(ctx); err != nil {
		return fmt.Errorf("drop collection: %w", err)
	}

	// Insert fixtures into mongoDB.
	if _, err := coll.InsertMany(ctx, documents); err != nil {
		return fmt.Errorf("insert many: %w", err)
	}

	return nil
}
