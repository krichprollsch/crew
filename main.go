package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"os"
	"os/signal"
	"syscall"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	exitOK   = 0
	exitFail = 1
)

// main starts interruptable context and runs the program.
func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGTERM, syscall.SIGINT)
	defer cancel()

	err := run(ctx, os.Args, os.Stdout, os.Stderr)
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(exitFail)
	}

	os.Exit(exitOK)
}

// run configures the flags and starts the HTTP API server.
func run(ctx context.Context, args []string, stdout, stderr io.Writer) error {

	// declare runtime flag parameters.
	// TODO use env var by default is set.
	flags := flag.NewFlagSet(args[0], flag.ExitOnError)
	var (
		addr         = flags.String("addr", "127.0.0.1:8080", "HTTP server address")
		mongodb      = flags.String("mongodb", "mongodb://127.0.0.1:27017", "MongoDB connection string")
		dbname       = flags.String("dbname", "crew", "Database name")
		loadFixtures = flags.Bool("fixtures", false, "Drop and reload fixtures in db")
	)
	// usage func declaration.
	flags.Usage = func() {
		fmt.Fprintf(stdout, "%s is an HTTP API server\n", args[0])
		fmt.Fprintf(stdout, "usage:\n")
		fmt.Fprintf(stdout, "\t%s [--dbname=crew] [--addr=127.0.0.1:8080]\truns the server.\n", args[0])
		fmt.Fprintf(stdout, "\t%s [--dbname=crew] --fixtures\tload db fixtures.\n", args[0])
	}
	if err := flags.Parse(args[1:]); err != nil {
		return err
	}

	// MongoDB connection
	dbconn, close, err := dbconn(ctx, stderr, *mongodb)
	if err != nil {
		return fmt.Errorf("mongodb conn: %w", err)
	}
	defer close()

	// Connect to MongoDB database.
	db := dbconn.Database(*dbname)

	// Load fixtures
	if *loadFixtures {
		if err := fixtures(ctx, db); err != nil {
			return fmt.Errorf("fixtures: %w", err)
		}

		return nil
	}

	// start the http api
	return runapi(ctx, stderr, db, *addr)
}

// dbconn connects to MongoDB uri.
// dbconn returns a close func for the connection. Error during close are logged
// into given stderr.
func dbconn(ctx context.Context, stderr io.Writer, uri string) (*mongo.Client, func(), error) {
	// force strict mode.
	apiopts := options.ServerAPI(options.ServerAPIVersion1).
		SetStrict(true).SetDeprecationErrors(true)
	cliopts := options.Client().ApplyURI(uri).SetServerAPIOptions(apiopts)
	db, err := mongo.Connect(ctx, cliopts)
	if err != nil {
		return nil, nil, err
	}

	// For DB disconnection, we want the context background to be sure all the
	// connections will be well closed.
	close := func() {
		if err := db.Disconnect(context.Background()); err != nil {
			fmt.Fprintln(stderr, fmt.Errorf("mongodb disconnect: %w", err))
		}
	}

	return db, close, nil
}
