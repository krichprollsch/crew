package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"

	"go.mongodb.org/mongo-driver/mongo"
	"goji.io"
	"goji.io/pat"

	"gitlab.com/krichprollsch/crew/api"
	"gitlab.com/krichprollsch/crew/talents"
)

// runapi starts http API server.
// Cancelling ctx will shutdown the http server gracefully.
func runapi(ctx context.Context, out io.Writer, db *mongo.Database, addr string) error {
	router := goji.NewMux()

	// routes declaration.
	router.HandleFunc(pat.Get("/v1/talents"), api.Handle(out, talents.Fetch(ctx, db)))
	router.HandleFunc(pat.Post("/v1/talents"), api.Handle(out, talents.Create(ctx, db)))

	srv := &http.Server{
		Addr:    addr,
		Handler: router,
		BaseContext: func(net.Listener) context.Context {
			return ctx
		},
	}

	// shutdown server on context cancelation
	go func(ctx context.Context, srv *http.Server) {
		<-ctx.Done()
		fmt.Fprintln(out, "server shutting down")
		if err := srv.Shutdown(ctx); err != nil {
			// context cancellation error is ignored.
			if !errors.Is(err, context.Canceled) {
				fmt.Fprintf(out, "server shutdown: %v\n", err)
			}
		}
	}(ctx, srv)

	fmt.Fprintf(out, "server listening: %s\n", addr)

	// ListenAndServe always returns a non-nil error.
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		return fmt.Errorf("server: %w", err)
	}
	fmt.Fprintln(out, "server shutdown")
	return nil
}
