.DEFAULT_GOAL := run

# self documented makefile
# see https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: run build fixtures test

build: ## Build the program
	@go build

run: mongo-start build ## Build and run the program
	@./crew

fixtures: mongo-start build ## Drop and reload db fixtures
	@./crew --fixtures

test: ## Run unit tests
	go test -v ./...

.PHONY: mongo-start mongo-stop

mongo-start:
	@docker-compose -f docker-compose.yml up -d mongodb

mongo-stop:
	@docker-compose -f docker-compose.yml stop mongodb
