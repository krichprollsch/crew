# Crew API

The crew HTTP API program exposes routes to manage talents.

This project aims to solve the crew.work [backend
challenge](https://github.com/crewdotwork/backend-challenge).

## Requirements

The program is implemented in [Go](https://go.dev/).

The program use also [Docker](https://docs.docker.com) and
[Docker composer](https://docs.docker.com/compose) to run a
[MongoDB](https://www.mongodb.com) local instance.

## Insert the fixtures

You can fill your mongo database with fixtures loaded from the [crew public
API](https://hiring.crew.work/v1/talents).

Be carreful, inserting fixtures will remove all the existing talents from DB.

```
$ make fixtures
```

## Run the program

```
$ make run
```

The server is accesible by default on http://127.0.0.1:8080.

### Fetch talents

```
$ curl http://127.0.0.1:8080/v1/talents
```

### Post talent

```
$ curl -XPOST -H "Content-type: application/json" 'http://127.0.0.1:8080/v1/talents' \
--data '{"id":"0123456789abcdefghijkl","firstName":"John","lastName":"Doe","job":"software engineer","location":"Paris, France","Stage":"🤝 Onsite Interview"}'
