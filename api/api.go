package api

import (
	"errors"
	"fmt"
	"io"
	"net/http"
)

// ErrApi represents an error returned by an HandlerFunc.
type ErrApi struct {
	// HTTP status code to return.
	Code int
	// Message returned to the client.
	Message string
	// Error logged by the server.
	Err error
}

func (e ErrApi) Error() string {
	if e.Err == nil {
		return ""
	}

	return e.Err.Error()
}

// errStatus returns an ErrApi corresponding to an http status code.
func errStatus(code int) ErrApi {
	msg := http.StatusText(code)

	return ErrApi{
		Code:    code,
		Message: msg,
		Err:     errors.New(msg),
	}
}

// Standard HTTP status codes.
var (
	NotFound       = errStatus(http.StatusNotFound)
	NotImplemented = errStatus(http.StatusNotImplemented)
)

func BadRequest(msg string) ErrApi {
	return ErrApi{
		Code:    http.StatusBadRequest,
		Message: msg,
		Err:     errors.New(msg),
	}
}

type HandlerFunc func(w http.ResponseWriter, r *http.Request) error

// Handle handles error returned by HandlerFunc and returns http.HandlerFunc
// func.
//
// If the error retuned is an ErrApi, the code and the message are written to
// the client. Else an HTTP error 500 is returned.
//
// Handle logs error into out.
func Handle(out io.Writer, f HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := f(w, r)
		// If no error is returned, nothing to do.
		if err == nil {
			return
		}

		w.Header().Set("Content-Type", "application/json")

		if errapi, ok := err.(ErrApi); ok {
			// The handler returned an ErrApi error, use give code and message.
			w.WriteHeader(errapi.Code)
			fmt.Fprintf(w, "%q", errapi.Message)
			return
		}

		// Generic error, use a 500 http error.
		fmt.Fprintf(out, "handler err: %v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "%q", http.StatusText(http.StatusInternalServerError))
	}
}
